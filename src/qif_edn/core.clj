(ns qif-edn.core
  (:require [clojure.string :refer [join]]
            [qif-edn.dialects :refer [dialects]]
            [clojure.edn :as edn]
            [clojure.pprint :refer [pprint]])
  (:import [java.time LocalDate]
           [java.time.format DateTimeFormatter])
  (:gen-class))

(defn formats [dialect]
  (get dialects dialect
       ; defaults:
       {:date "yyyyMMdd"
        :amount ""}))

(defn parse-amount [dialect a]
  (let [amount-format ((formats dialect) :amount)]
    (/ (->> a
            (re-find amount-format)
            (rest)
            (join "")
            (edn/read-string))
       100)))

(defn parse-date [dialect a]
  (let [date-format ((formats dialect) :date)]
    (LocalDate/parse a (DateTimeFormatter/ofPattern date-format))))

(defn detail-code->generic [dialect [dc d]]
  (case dc
    :P {:payee-name d}
    :M {:description d}
    :D {:value-date (parse-date dialect d)}
    :A {:payee-address d}
    :T {:amount (parse-amount dialect d)}
    :N {:transaction-reference d}
    {dc d}))

(defn parse-qif [account-info dialect lines]
  (let [header (re-find #"!Type:(.*)" (first lines))
        records (if header (rest lines) lines)]
    (assoc account-info
           :header (second header)
           :transactions
           (into []
                 (comp
                  (partition-by #(= % "^")) ; group statements
                  (take-nth 2)              ; remove statement separators
                  (map (partial map (partial re-matches #"([DTMCNPALFSE$%NYIQO$]|X[AIECRTSN#$F])(.*)")))
                  (map (partial map (fn [[_ dc d]] (detail-code->generic dialect [(keyword dc) d]))))
                  (map (partial apply merge-with (fn [a b] (str a "\n" b)))))
                 records))))

(defn import-statements [account-info dialect qif-file]
  (with-open [rdr (clojure.java.io/reader qif-file)]
    ;; return as vector with one element, because one qif file is one statement
    [(parse-qif (merge {:bank-name (name dialect)
                        :account-id "Credit Card"}
                       account-info)
                dialect (line-seq rdr))]))

(defn -main
  ([filename]
   (-main :abnamro filename))
  ([dialect-name filename]
   (let [dialect (keyword dialect-name)]
     (pprint (import-statements {:statement-id filename} dialect filename)))))
