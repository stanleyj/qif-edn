(ns qif-edn.dialects)

(def dialects
  {:abnamro {:date "dd/MM/yyyy"
               :amount #"^(-?)\D*(\d{1,3})(?:,([0-9]{3}))*\.([0-9]+$)"}})
