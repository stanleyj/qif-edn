{
  description = "Utility to convert QIF files into an EDN structure";

  inputs = {
    nixpkgs.url = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    clj-nix.url = "github:jlesquembre/clj-nix";
  };

  outputs = { self, nixpkgs, flake-utils, clj-nix }:

    flake-utils.lib.eachDefaultSystem (system: {

      packages = {

        default = clj-nix.lib.mkCljApp {
          pkgs = nixpkgs.legacyPackages.${system};
          modules = [
            # Option list:
            # https://jlesquembre.github.io/clj-nix/options/
            {
              projectSrc = ./.;
              name = "qif-edn";
              main-ns = "qif-edn.core";

              nativeImage.enable = true;
              nativeImage.graalvm = nixpkgs.legacyPackages.${system}.graalvm-ce;

              # customJdk.enable = true;
            }
          ];
        };

      };
    });
}
